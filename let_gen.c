//generate a random letter
//William Doyle
//Late December 2019

#include <stdio.h>	//used to output result
#include <stdlib.h>	//used for random functions -- srand() and rand()
#include <time.h>	//used to seed random generator

int main (){

	char let;				//the char to generate
	srand(time(0));				//seed random with current time
	let = rand() % ('Z' - 'A' + 1) + 'A';	//generate random letter and assign it to let variable
	printf("Your letter is : %c\n", let);	//output the generated value with a short message for context

}
